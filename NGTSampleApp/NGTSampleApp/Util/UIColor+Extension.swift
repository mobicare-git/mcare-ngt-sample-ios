//
//  UIColor+Extension.swift
//  NGTSampleApp
//
//  Created by Fernanda FC. Carvalho on 18/03/22.
//

import UIKit

extension UIColor {
    
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if hexString.hasPrefix("#") {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    
    func toHexString() -> String {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0
        getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        let rgb:Int = (Int)(red*255)<<16 | (Int)(green*255)<<8 | (Int)(blue*255)<<0
        return String(format:"#%06x", rgb)
    }
    
    static let primary = UIColor(named: "primary") ?? UIColor(hexString: "#1855E5")
    static let primaryLight = UIColor(named: "primaryLight") ?? UIColor(hexString: "#EFF5FC")
    static let textLight = UIColor(named: "textLight") ?? UIColor(hexString: "#FFFFFF")
    static let textDark = UIColor(named: "textDark") ?? UIColor(hexString: "#4a4646")
}
