//
//  String+Extension.swift
//  NGTSampleApp
//
//  Created by Fernanda FC. Carvalho on 21/03/22.
//

import Foundation

extension String {
    var withoutWhiteSpaces: String {
        return self.replacingOccurrences(of: "^\\s+|\\s+|\\s+$",
                                         with: "",
                                         options: .regularExpression)
    }
}
