//
//  NGTSampleUtils.swift
//  NGTSampleApp
//
//  Created by Fernanda FC. Carvalho on 16/03/22.
//

import Foundation
import UIKit
import Firebase

final class NGTSampleUtils {
    static func tokenToString(token: Data) -> String {
        let tokenString = token.map { String(format: "%02.2hhx", $0) }.joined() //"%02X"
        return tokenString
    }
    
    static func getUUID() -> String? {
        guard let uuid = UIDevice.current.identifierForVendor?.uuidString else { return nil }
        return uuid
    }
    
    static func getDemoUserKey() -> String {
        let prefix = "demo-"
        guard let deviceId = getUUID() else {
            let randomNumber = Int.random(in: 0..<200)
            let randomString = String(randomNumber)
            return prefix + randomString
        }
        let data = (deviceId).data(using: String.Encoding.utf8)
        let base64 = data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        let trimmed = base64.prefix(8)
        return prefix + trimmed
    }
    
    static func getFcmToken() -> String? {
        let token = Messaging.messaging().fcmToken
        return token != nil && !token!.isEmpty ? token : nil
    }
    
    static func getApnsToken() -> Data? {
        return Messaging.messaging().apnsToken
    }
    
    static func createLocalUrl(forImageNamed name: String) -> URL? {
        
        let fileManager = FileManager.default
        let cacheDirectory = fileManager.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        let url = cacheDirectory.appendingPathComponent("\(name).png")
        
        guard fileManager.fileExists(atPath: url.path) else {
            guard
                let image = UIImage(named: name),
                let data = image.pngData()
            else { return nil }
            
            fileManager.createFile(atPath: url.path, contents: data, attributes: nil)
            return url
        }
        
        return url
    }
    
    static func getCampaignID() -> Int {
        return 2022
    }
}
