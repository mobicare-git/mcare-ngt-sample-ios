//
//  UIApplication+Extension.swift
//  NGTSampleApp
//
//  Created by Fernanda FC. Carvalho on 21/03/22.
//

import UIKit

extension UIApplication {
    
    public class func shortVersionString() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
}
