//
//  ModalView.swift
//  NGTSampleApp
//
//  Created by Fernanda FC. Carvalho on 14/03/22.
//

import Foundation
import UIKit

final class ModalView: UIView {
    
    @IBOutlet weak var curlTextView: UITextView!
    @IBOutlet weak var footerLabel: UILabel!
    @IBOutlet weak var copyButton: UIButton!
    @IBOutlet weak var copiedLabel: UILabel!
    
    private var cURLString: String?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
    }
    
    func setCurl(cURL: String) {
        cURLString = cURL
        curlTextView.text = cURL
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
    }
               
    @IBAction func closeModal(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func copyCurl(_ sender: Any) {
        UIPasteboard.general.string = curlTextView.text
        addCopyAnimation()
    }
}

fileprivate extension ModalView {
    
    func loadNib() {
        if let nib = Bundle.main.loadNibNamed("ModalView", owner: self),
            let nibView = nib.first as? UIView {
            nibView.frame = bounds
            nibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            addSubview(nibView)
        }
    }
    
    func addCopyAnimation() {
        copiedLabel.alpha = 0
        self.copiedLabel.isHidden = false
        UIView.animate(withDuration: 2.5) {
            self.copyButton.alpha = 0.5
            self.copyButton.isEnabled = false
            self.copiedLabel.alpha = 1
        } completion: { _ in
            self.copyButton.alpha = 1
            self.copyButton.isEnabled = true
            self.copiedLabel.isHidden = true
        }
    }
}
