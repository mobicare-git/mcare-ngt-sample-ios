//
//  ViewController.swift
//  NGTSampleApp
//
//  Created by Fernanda FC. Carvalho on 14/03/22.
//

import UIKit
import NGTSdk

enum NotificationMediaType {
    case none
    case image
}

class ViewController: UIViewController {
    
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var unregisterBtn: UIButton!
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageField: UITextView!
    @IBOutlet weak var imageBtnCheck: UIButton!
    @IBOutlet weak var urlField: UITextField!
    @IBOutlet weak var sendNotificationBtn: UIButton!
    @IBOutlet weak var requestDetailLink: UIButton!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var userKeyLabel: UILabel!
    @IBOutlet weak var keyCopyLabel: UILabel!
    
    
    private var service: MainService = MainService()
    private var isUserRegistered: Bool = false {
        didSet {
            updateButtonsState()
        }
    }
    private var notificationMediaType: NotificationMediaType = .none
    private let defaultImageUrl = "https://ngt.mobicare.com.br/img/push.png"
    private let defaultMessage = "Olá, seja bem vindo ao App demo do Mobi Notification Gateway!"
    private var requestCurl: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        initUI()
        registerNotifications()
        
        //TODO: USAR IMG FIXA NO APP
    }
    
    //MARK: NGT ACTIONS
    
    @IBAction func register(_ sender: Any) {
        resignViewFirstResponder()
        guard let fcmToken = NGTSampleUtils.getFcmToken(),
              let apnsToken = NGTSampleUtils.getApnsToken() else {
                  print("---- Failed to get tokens ----")
                  return
        }
        
        showActivityIndicator()
        
        //Register with completionHandler implementation
        NGTManager.sharedInstance.registerForPushNotification(userKey: NGTSampleUtils.getDemoUserKey(), token: apnsToken, fcmToken: fcmToken, completionHandler: { [weak self] (error) in
            
            self?.removeActivityIndicator()
            if error == nil {
                self?.isUserRegistered = true
                self?.showAlert(withTitle: "Registro efetuado", andMessage: "O usuário foi registrado para o recebimento de push notifications")
            } else {
                self?.showAlert(withTitle: "Registro não efetuado", andMessage: "Houve uma falha ao registrar o usuário para o recebimento de push notifications")
            }
        })
    }
    
    @IBAction func unregister(_ sender: Any) {
        resignViewFirstResponder()
        guard let fcmToken = NGTSampleUtils.getFcmToken(),
              let apnsToken = NGTSampleUtils.getApnsToken() else {
                  print("---- Failed to get tokens ----")
                  return
              }
        
        showActivityIndicator()
        
        //Unregister with completionHandler implementation
        NGTManager.sharedInstance.unregisterForPushNotification(userKey: NGTSampleUtils.getDemoUserKey(), token: apnsToken, fcmToken: fcmToken, completionHandler: { [weak self] (error) in
            
            self?.removeActivityIndicator()
            
            if error == nil {
                self?.isUserRegistered = false
                self?.showAlert(withTitle: "Registro removido", andMessage: "O registro do usuário para o recebimento de push notifications foi removido")
                self?.resetInitialState()
            } else {
                self?.showAlert(withTitle: "Cancelamento de registro não efetuado", andMessage: "Houve uma falha ao remover o registro do usuário para o recebimento de push notifications")
            }
        })
    }
    
    //MARK: DEMO ACTIONS
    
    @IBAction func sendDefaultNotification(_ sender: Any) {
        resignViewFirstResponder()
        showActivityIndicator()
        
        service.sendNotification(title: getTitleParameter() , message: messageField.text, imageUrl: getImageUrlParameter()) { [weak self] (result) in
            self?.removeActivityIndicator()
            
            switch result {
            case .success(let curl):
                self?.requestCurl = curl
                self?.requestDetailLink.isHidden = false
                self?.showAlert(withTitle: "Ok", andMessage: "Sua notificação foi enviada")
            case .failure(let error):
                self?.showAlert(withTitle: "Falha ao enviar notificação", andMessage: error.localizedDescription)
            }
        }
    }
    
    @IBAction func openSendDetails(_ sender: Any) {
        resignViewFirstResponder()
        let modalView = ModalView(frame: self.view.bounds)
        if let cURL = requestCurl {
            modalView.setCurl(cURL: cURL)
        }
        self.view.addSubview(modalView)
    }
    
    @IBAction func checkImageNotification(_ sender: Any) {
        resignViewFirstResponder()
        notificationMediaType =  notificationMediaType == .image ? .none : .image
        updateCheckboxState()
    }
    
    @IBAction func copyUserKey(_ sender: Any) {
        UIPasteboard.general.string = NGTSampleUtils.getDemoUserKey()
        addKeyCopyAnimation()
    }
    
}

//MARK: TEXTVIEW DELEGATE

extension ViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        if newText.withoutWhiteSpaces.count == 0 {
            textView.text = defaultMessage
        }
        return true
    }
}

fileprivate extension ViewController {
    
    func initUI() {
        messageField.delegate = self
        addTapGesture()
        setUserKeyLabel()
        setButtonsColors()
        updateButtonsState()
        updateCheckboxState()
        updateUrlFieldState()
        requestDetailLink.isHidden = true
        messageView.layer.borderWidth = 1
        if #available(iOS 13.0, *) {
            messageView.layer.borderColor = UIColor.systemGray5.cgColor
        } else {
            messageView.layer.borderColor = UIColor.lightGray.cgColor
        }
        versionLabel.text = "SDK v0.2.4 | Demo v" + UIApplication.shortVersionString()
    }
    
    func setUserKeyLabel() {
        userKeyLabel.text = "Userkey: \(NGTSampleUtils.getDemoUserKey())"
    }
    
    func setButtonsColors() {
        registerBtn.setTitleColor(.white, for: .normal)
        registerBtn.setTitleColor(.primary.withAlphaComponent(0.7), for: .disabled)
        unregisterBtn.setTitleColor(.white, for: .normal)
        unregisterBtn.setTitleColor(.primary.withAlphaComponent(0.7), for: .disabled)
        sendNotificationBtn.setTitleColor(.white, for: .normal)
        sendNotificationBtn.setTitleColor(.primary.withAlphaComponent(0.7), for: .disabled)
    }
    
    func updateButtonsState() {
        registerBtn.isEnabled = !isUserRegistered
        registerBtn.backgroundColor = registerBtn.isEnabled ? .primary : .primaryLight
        unregisterBtn.isEnabled = isUserRegistered
        unregisterBtn.backgroundColor = unregisterBtn.isEnabled ? .primary : .primaryLight
        sendNotificationBtn.isEnabled = isUserRegistered
        sendNotificationBtn.backgroundColor = sendNotificationBtn.isEnabled ? .primary : .primaryLight
    }
    
    func updateCheckboxState() {
        if let btnImage = getCheckBoxImage() {
            imageBtnCheck.setImage(btnImage, for: .normal)
        }
        updateUrlFieldState()
    }
    
    func getCheckBoxImage() -> UIImage? {
        let image = notificationMediaType == .image ? UIImage(named: "circle-filled-icon") : UIImage(named: "circle-outline-icon")
        return image
    }
    
    func updateUrlFieldState() {
        urlField.isHidden = notificationMediaType == .none ? true : false
        urlField.text = defaultImageUrl
    }
    
    func resetInitialState() {
        titleField.text = ""
        messageField.text = defaultMessage
        notificationMediaType = .none
        updateCheckboxState()
        requestDetailLink.isHidden = true
    }
    
    func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(resignViewFirstResponder))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func resignViewFirstResponder() {
        self.view.endEditing(true)
    }
    
    func isValidUrl() -> Bool {
        return urlField.text != nil && !urlField.text!.isEmpty
    }
    
    func getTitleParameter() -> String? {
        if let text = titleField.text,
           !text.withoutWhiteSpaces.isEmpty {
            return text
        }
        return nil
    }
    
    func getImageUrlParameter() -> String? {
        if notificationMediaType == .image {
            let urlMedia = isValidUrl() ? urlField.text! : defaultImageUrl
            urlField.text = urlMedia
            return urlMedia
        }
        return nil
    }
    
    func addKeyCopyAnimation() {
        keyCopyLabel.alpha = 0
        keyCopyLabel.isHidden = false
        UIView.animate(withDuration: 1) {
            self.keyCopyLabel.alpha = 1
        } completion: { _ in
            self.keyCopyLabel.isHidden = true
        }
    }
    
    //MARK: - NOTIFICATION CENTER
    
    func registerNotifications() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    //MARK: - KEYBOARD HANDLERS
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height/2
            }
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        if let _ = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }
        }
    }
    
    //MARK: LOADING
    
    func showActivityIndicator() {
        DispatchQueue.main.async {
            ActivityIndicatorView.showActivityIndicatorView(onView: self.navigationController?.view ?? self.view)
        }
    }
    
    func removeActivityIndicator() {
        DispatchQueue.main.async {
            ActivityIndicatorView.removeActivityIndicatorView()
        }
    }
    
    //MARK: Alert
    
    func showAlert(withTitle title: String, andMessage message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK" , style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func showCallbackAlert(withTitle title: String, andMessage message: String, completion: @escaping () -> ()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Tentar novamente", style: .default, handler: { (action) in
            completion()
        }))
        
        self.present(alert, animated: true)
    }
}



