//
//  MainService.swift
//  NGTSampleApp
//
//  Created by Fernanda FC. Carvalho on 17/03/22.
//

import Foundation

enum NetworkError: Error {
    case badURL
    case dataTaskError(Error)
    case dataParseError
}

final class MainService: NSObject {
    
    func sendNotification(title: String? = nil, message: String, imageUrl: String? = nil, completionHandler: @escaping (Result<String, NetworkError>) -> Void) {
        requestAuthorization { [weak self] (result) in
            switch result {
            case .success(let auth):
                if let token = auth.accessToken {
                    self?.requestNotification(token: token, title: title, message: message, imageUrl: imageUrl, completionHandler: completionHandler)
                } else {
                    completionHandler(.failure(.dataParseError))
                }
            case .failure(let error):
                completionHandler(.failure(.dataTaskError(error)))
            }
        }
    }
}

fileprivate extension MainService {
    func createRequestWithDefaultHeaders(forUrl url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue("*/*", forHTTPHeaderField: "Accept")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        
        return request
    }
    
    func requestAuthorization(completionHandler: @escaping (Result<Authentication, NetworkError>) -> Void) {
        
        let tokenUrl = "https://ngt-p.mobicare.com.br/mcare-ngt/v1/notification/oauth2/token"
        guard let url = URL(string: tokenUrl) else {
            completionHandler(.failure(.badURL))
            return
        }
        
        let model = AuthRequestModel()
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        
        do {
            let body = try encoder.encode(model)
            var request = createRequestWithDefaultHeaders(forUrl: url)
            request.httpMethod = "POST"
            request.addValue("\(body.count)", forHTTPHeaderField: "Content-Length")
            request.httpBody = body
            
            print(" \n ---- Getting authentication token ----  \n ")
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                DispatchQueue.main.async {
                    self.printcURL(from: request)
                    
                    if let err = error {
                        print(err.localizedDescription)
                        completionHandler(.failure(.dataTaskError(err)))
                        return
                    }
                    
                    guard let responseData = data else {
                        completionHandler(.failure(.dataParseError))
                        return
                    }
                    
                    do {
                        let auth = try JSONDecoder().decode(Authentication.self, from: responseData)
                        completionHandler(.success(auth))
                    } catch  {
                        completionHandler(.failure(.dataParseError))
                    }
                }
                
            }.resume()
            
        } catch  {
            completionHandler(.failure(.dataParseError))
        }
    }
    
    func getNotificationParameters(title: String?, imageUrl: String?) -> [NotificationParameter]? {
        var parameters = [NotificationParameter]()
        if let titleText = title {
            let parameter = NotificationParameter(key: "title", value: titleText)
            parameters.append(parameter)
        }
        if let urlString = imageUrl {
            let parameter = NotificationParameter(key: "banner-url", value: urlString)
            parameters.append(parameter)
        }
        return parameters.count > 0 ? parameters : nil
    }
    
    func requestNotification(token: String, title: String?, message: String, imageUrl: String? = nil, completionHandler: @escaping (Result<String, NetworkError>) -> Void) {
        
        let notificationUrl = "https://ngt-p.mobicare.com.br/mcare-ngt/v2/notification"
        guard let url = URL(string: notificationUrl) else {
            completionHandler(.failure(.badURL))
            return
        }
        
        var model = NotificationModel(userKey: NGTSampleUtils.getDemoUserKey(), message: message)
        if let parameters = getNotificationParameters(title: title, imageUrl: imageUrl) {
            model.parameters = parameters
        }
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        
        do {
            let body = try encoder.encode(model)
            var request = createRequestWithDefaultHeaders(forUrl: url)
            request.httpMethod = "POST"
            request.addValue("\(NGTSampleUtils.getCampaignID())", forHTTPHeaderField: "Campaign")
            request.addValue(token, forHTTPHeaderField: "Authorization")
            request.addValue("\(body.count)", forHTTPHeaderField: "Content-Length")
            request.httpBody = body
            
            print(" \n ---- Sending Notification ----  \n ")
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                self.printcURL(from: request)
                
                DispatchQueue.main.async { [self] in
                    if let err = error {
                        print(err.localizedDescription)
                        completionHandler(.failure(.dataTaskError(err)))
                        return
                    } else {
                        print(" \n ---- Notification Sent ----  \n ")
                        completionHandler(.success(self.getcURL(from: request)))
                    }
                }
                
            }.resume()
            
        } catch  {
            completionHandler(.failure(.dataParseError))
        }
    }
    
    func printcURL(from urlRequest: URLRequest) {
        let curl = getcURL(from: urlRequest)
        
        print(" \n ")
        print(" --------------------------------------- ")
        print(" \n ")
        print(curl)
        print(" \n ")
        print(" --------------------------------------- ")
        print(" \n ")
    }
    
    func getcURL(from urlRequest: URLRequest) -> String {
        guard let url = urlRequest.url else { return ""}
        var baseCommand = #"curl "\#(url.absoluteString)""#
        if urlRequest.httpMethod == "HEAD" {
            baseCommand += " --head"
        }
        
        var command = [baseCommand]
        if let method = urlRequest.httpMethod, method != "GET" && method != "HEAD" {
            command.append("-X \(method)")
        }
        
        if let headers = urlRequest.allHTTPHeaderFields {
            for (key, value) in headers where key != "Cookie" {
                command.append("-H '\(key): \(value)'")
            }
        }
        
        if let data = urlRequest.httpBody, let body = String(data: data, encoding: .utf8) {
            command.append("-d '\(body)'")
        }
        
        return command.joined(separator: " \\\n\t")
    }
    
}

