//
//  AppDelegate.swift
//  NGTSampleApp
//
//  Created by Fernanda FC. Carvalho on 14/03/22.
//

import UIKit
import Firebase
import NGTSdk

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
        window?.makeKeyAndVisible()
        firebaseConfigs()
        ngtConfigs()
        return true
    }
    
    // MARK: Firebase setup
    
    func firebaseConfigs() {
        FirebaseApp.configure()
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: { granted, error in
                print("Notification Permission granted: \(granted)")
            })
        
        UIApplication.shared.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
    }
    
    // MARK: NGT setup
    
    func ngtConfigs() {
        //Init SDK with Authorization Key
        NGTManager.sharedInstance.initSdk(withAuthorizationKey: "YeymmlBi1nGPeV9hgUz10CCpZjbSDx4S")
        
        //Set the delegate if you want to listen to register events
        //(You can handle this events individually by callbacks when calling register and unregister methods, if you prefer)
        NGTManager.sharedInstance.delegate = self
    }

}

//MARK: - Push Notification

extension AppDelegate: UNUserNotificationCenterDelegate {
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("\n ---- APNS TOKEN: \(NGTSampleUtils.tokenToString(token: deviceToken)) ---- \n")
        
        //Set APNS token
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("\n ---- Failed to get registration token. Error: \(error) ---- \n")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print("\n ---- didReceiveNotificationResponse: \n\(userInfo) ---- \n")
        
        //Notify NGT that user interacted with the push notification
        NGTManager.sharedInstance.handleNotificationResponse(response: response)
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print("\n ---- willPresentNotification: \n\(userInfo) ---- \n")
        
        //Notify NGT that a remote notification was received in foreground
        //(Shouldn't be called here if you already called it in NotificationServiceExtension)
        //NGTManager.sharedInstance.didReceiveNotification(userInfo: userInfo)
        
        completionHandler([.alert, .badge, .sound])
    }
}

//MARK: - Messaging delegate

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("---- FCM TOKEN: \(String(describing: fcmToken)) ----")
        
        guard let fcmToken = NGTSampleUtils.getFcmToken(),
              let apnsToken = NGTSampleUtils.getApnsToken() else {
                  return
        }
        
        //Notify NGT that fcmToken was refreshed
        NGTManager.sharedInstance.registerForPushNotification(userKey: NGTSampleUtils.getDemoUserKey(), token: apnsToken, fcmToken: fcmToken)
    }
}

//MARK: - NGT delegate

extension AppDelegate: NGTDelegate {
    func didRegisterForPushNotifications() {
        print("---- NGT SDK Register completed ----")
    }
    
    func didFailRegisteringForPushNotifications(error: NGTRequestError) {
        print("---- NGT SDK Register failed ----")
    }
    
    func didUnregisterForPushNotifications() {
        print("---- NGT SDK Unregister completed ----")
    }
    
    func didFailUnregisteringForPushNotifications(error: NGTRequestError) {
        print("---- NGT SDK Unregister failed ----")
    }
    
}



