//
//  NotificationModel.swift
//  NGTSampleApp
//
//  Created by Fernanda FC. Carvalho on 17/03/22.
//

import Foundation

struct NotificationModel: Encodable {
    var userKey: String
    var message: String
    var mutableContent: Bool = true
    var parameters: [NotificationParameter]? = nil
}


struct NotificationParameter: Encodable {
    var key: String
    var value: String
    var send: Bool = true
}
