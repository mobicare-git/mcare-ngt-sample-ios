//
//  AuthModel.swift
//  NGTSampleApp
//
//  Created by Fernanda FC. Carvalho on 17/03/22.
//

import Foundation

struct AuthRequestModel: Encodable {
    var clientId: String = "gSeIEyKirlmIENIQ6H2H7GpUOHi2YWOw"
    var clientSecret: String = "p9jt31JLxuB8afkzNvHDU5l7fXpcuZIY"
    var grantType: String = "client_credentials"
    
    enum CodingKeys: String, CodingKey {
        case clientId = "client_id"
        case clientSecret = "client_secret"
        case grantType = "grant_type"
    }
}

struct Authentication: Decodable {
    var tokenType: String?
    var accessToken: String?
    var expiresIn: Int?
    
    enum CodingKeys: String, CodingKey {
        case tokenType = "token_type"
        case accessToken = "access_token"
        case expiresIn = "expires_in"
    }
}
