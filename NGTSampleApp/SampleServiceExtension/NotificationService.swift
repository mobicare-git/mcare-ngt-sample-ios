//
//  NotificationService.swift
//  SampleServiceExtension
//
//  Created by Fernanda FC. Carvalho on 22/03/22.
//

import UserNotifications
import UIKit
import NGTSdk

class NotificationService: UNNotificationServiceExtension {
    
    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?
    
    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        let userInfo = request.content.userInfo
        
        //Notify NGT that a remote notification was received
        NGTManager.sharedInstance.didReceiveNotification(userInfo: userInfo)
        
        defer {
            contentHandler(bestAttemptContent ?? request.content)
        }

        //Optional, implement it only if you want custom push notifications
        
        if let title = userInfo["title"] as? String {
            bestAttemptContent?.title = title
        }
        
        guard let attachment = request.attachment else { return }
        
        bestAttemptContent?.attachments = [attachment]
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
}


