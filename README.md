# Mobi Push Notification
O que você vê em primeiro lugar quando pega seu celular? A  notificação push mais recente recebida em seu dispositivo por um de seus aplicativos. Dependendo de quão interessante é, você interage com ele ou o ignora.

Hoje, o push notification é crucial para qualquer estratégia de marketing móvel. As notificações push são mensagens de texto e atingem apenas os usuários que instalaram seu aplicativo. Eles melhoram a experiência do cliente e o envolvimento do usuário. 

Com notificações push, os desenvolvedores de aplicativos Android e iOS direcionam o tráfego e aumentam a fidelidade, fornecendo informações no lugar certo e na hora certa. 

Se você não enviar mensagens aos usuários quando eles instalarem seu aplicativo, estará desperdiçando 95% de seus gastos com aquisição. Isso porque os usuários que recebem notificações nos primeiros 90 dias têm uma taxa de retenção 180% maior do que aqueles que não recebem. (de onde vem esse dado?)
Mais de 75% dos downloads de aplicativos resultam em um único aplicativo aberto. Os usuários abrem o aplicativo uma vez e nunca mais voltam. 

Portanto, não perca esta ótima ferramenta de marketing para aumentar seu público! O Mobi Push Notification é a ferramenta ideal!

[![NPM Version][npm-image]][npm-url]
[![Build Status][travis-image]][travis-url]
[![Downloads Stats][npm-downloads]][npm-url]


# O Mobi Notification Gateway fornece a melhor solução de notificações push para automatizar os seguintes casos de uso:
 
* Dê as boas-vindas aos usuários depois que eles instalarem seu aplicativo : ("Olá Antonio! Seja bem vindo, vamos conhecer o App …!")
* Informações em tempo real (“Sua solicitação acabou de ser aprovada!”)
* Envie mensagens contextuais que direcionam para uma determinada funcionalidade do seu App. (“Sua reserva começa em 15 minutos, quer fazer o seu pré-check-in agora?”)
* Direcionamento de compradores (“Venha conhecer a nova coleção de verão!”)
* Converta carrinhos abandonados (“Você está a apenas um clique de concluir sua compra!”)
* Engajar usuários inativos (“Sentimos sua falta…”)

<img src="https://www.mobicare.com.br/wp-content/uploads/2022/06/testpush01.png" width="400">
 
## Configurando o Mobi Push Notification no seu App

O Mobi Push Notification utiliza o SDK Admin do Firebase para interagir com os servidores FCM, e utilizamos o protocolo  API FCM HTTP v1, que é a opção de protocolo mais atualizada, com autorização mais segura e recursos flexíveis de mensagens entre plataformas. O SDK Admin do Firebase é baseado nesse protocolo e oferece todas as vantagens dele. Os novos recursos geralmente são adicionados apenas à API HTTP v1. Por isso, recomendamos a utilização dela API na maioria dos casos de uso.

Para isso, será necessário ter na sua conta do Firebase, uma conta de serviço ativa para utilizar o Cloud Messaging.

### O que é uma conta de serviço Firebase?

Uma conta de serviço representa uma identidade de serviço do Google Cloud, como o código que é executado nas VMs do Compute Engine, aplicativos do App Engine ou sistemas executados fora do Google. Saiba mais sobre contas de serviço.

Políticas da organização podem ser usadas para proteger contas de serviço e bloquear recursos arriscados de conta de serviço, como IAM Grants automático, criação/upload de chaves ou a criação de contas de serviço inteiramente. Saiba mais sobre políticas da organização da conta de serviço.


### O que vou precisar fazer?
Para usar o Mobi Push Notification, você precisará de:

* Passo 1: Ter um projeto do Firebase;
* Passo 2: Possuir uma  conta de serviço do SDK Admin do Firebase;
* Passo 3: Gerar um arquivo de configuração com as credenciais da sua conta de serviço;
* Passo 4: Configurar o Portal Administrativo do Mobi Push Notification;
* Passo 5: Configurar o SDK no seu App iOS.

Caso você não possua um projeto no Firebase criado, siga a partir do passo 1. Caso já possua um projeto criado no Firebase, siga para o passo 2.


#### Passo 1: Criando o seu projeto Firebase

Se você ainda não tem um projeto do Firebase, crie um no Console do Firebase. Consulte Noções básicas sobre projetos do Firebase para mais informações.


No [Console do Firebase](https://console.firebase.google.com/), clique em Criar um projeto.

<img src="https://ngt.mobicare.com.br/docs/images/image11.png" width="400">
Fig. 1: Criando um novo projeto


Informe o nome do seu projeto, aceite os termos de uso do Firebase e clique em Continuar.

<img src="https://ngt.mobicare.com.br/docs/images/image7.png" width="400">
Fig. 2: Criando um novo projeto - nome do projeto


Clique novamente em Continuar.

<img src="https://ngt.mobicare.com.br/docs/images/image2.png" width="400">
Fig. 3: Criando um novo projeto - Analytics


Selecione o local do Google Analytics, aceite os termos de uso e clique em Criar projeto.

<img src="https://ngt.mobicare.com.br/docs/images/image1.png" width="400">
Fig. 4: Criando um novo projeto - conclusão



#### Passo 2: Possuir uma conta de serviço configurada

É necessário uma  conta de serviço do SDK Admin do Firebase para se comunicar com o Firebase. Ela é gerada automaticamente quando você cria um projeto do Firebase ou adiciona a plataforma a um projeto do Google Cloud.

No console do seu projeto, clique em Configurações, depois na Aba Cloud Messaging e certifique-se de que a API Firebase Cloud Messaging (V1) esteja ativada como mostra a figura 5.

<img src="https://ngt.mobicare.com.br/docs/images/image9.png" width="400">
Fig. 5: API Firebase Cloud Messaging (V1) ativada



#### Passo 3: Gerar um arquivo de configuração com as credenciais da sua conta de serviço

Uma vez tendo o seu projeto criado e o Cloud Messaging ativado, automaticamente sua conta de serviço já estará criada. Basta agora, gerar uma chave privada para que seja configurado no Portal Administrativo do Mobi Push Notification.

Na aba Contas de serviço, com a SDK Admin do Firebase selecionado, clique no botão Gerar nova chave privada.

<img src="https://ngt.mobicare.com.br/docs/images/image5.png" width="400">
Fig. 6: Chave privada do SDK Admin do Firebase


Confirme a geração da chave privada clicando no botão Gerar chave.

<img src="https://ngt.mobicare.com.br/docs/images/image8.png" width="400">
Fig. 7: Chave privada do SDK Admin do Firebase


Ao clicar em Gerar chave, será feito o download de um arquivo JSON para a área de downloads do seu computador. O arquivo possuirá um conteúdo parecido com esse da figura 8.

<img src="https://ngt.mobicare.com.br/docs/images/image6.png" width="400">
Fig. 8: JSON Chave privada

ATENÇÃO: Guarde bem esse arquivo, ele precisará ser enviado para Portal Administrativo do Mobi Push Notification para concluir a configuração da sua conta.



#### Passo 4: Configurar o Portal Administrativo do Mobi Push Notification

Precisamos agora finalizar a configuração do seu projeto para que ele possa começar a utilizar o Mobi Push Notification.

Acesse o Portal Administrativo do [Mobi Push Notification](https://ngt.mobicare.com.br/admin) e faça o seu login.

Na sua área de trabalho, vá em Configurar meu App.

Na opção Conta de Serviço Firebase, envie o arquivo JSON da chave privada gerada no passo 3 e clique em confirmar.

Tudo pronto! Seu App já está apto a começar a usar o Mobi Push Notification!



#### Passo 5: Configurar o SDK no seu App iOS

##### Primeiros passos
* Obter as credenciais de acesso ao artifactory. Pegue na área Credenciais no Portal Administrativo do Mobi Push Notification.
* Obter o token de autorização para uso da api. Pegue na área Credenciais no Portal Administrativo do Mobi Push Notification.

##### Requisitos
* Certificado de push iOS;
* Target mínimo iOS 11;
* Firebase Messaging.

##### Glossário
* APP_AUTHORIZATION: é o token de autorização para uso da api

##### PushNotifications capability
Seu app precisa ter essa capability. Para saber mais sobre como adicionar [Acesse](https://developer.apple.com/documentation/usernotifications/registering_your_app_with_apns/)

##### Configuração do Firebase
Para configurar o firebase e habilitar seu app para o recebimento de push notifications acesse [Configurar um app cliente do Firebase Cloud Messaging em plataformas da Apple](https://firebase.google.com/docs/cloud-messaging/ios/client?hl=pt-br)

##### Instalação
###### Configuração do SDK

1. Acesse o Portal Administrativo do Mobi Push Notification, faça o download do framework SDK iOS e instale em seu aplicativo;
<img src="https://ngt.mobicare.com.br/docs/images/image4.png" width="400">
<img src="https://ngt.mobicare.com.br/docs/images/image-embed-app.png" width="400">
<img src="https://ngt.mobicare.com.br/docs/images/image-embed-extension.png" width="400">

2. No AppDelegate, importe o framework e habilite o SDK usando sua chave de acesso.

```sh
import NGTSdk
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
	func application(_ application: UIApplication, didFinishLaunchingWithOptions
	launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		//Inicialize o SDK com sua chave
		NGTManager.sharedInstance.initSdk(withAuthorizationKey: "<your-key>")
		//Implemente se quiser observar os eventos de registro
		//(Você pode, se preferir, tratar esses eventos posteriormente através de callbacks nas chamadas dos métodos register e unregister)
		NGTManager.sharedInstance.delegate = self
		return true        
	}
}
```

Implemente se tiver assinado o protocolo

```sh
extension AppDelegate: NGTDelegate {
	func didRegisterForPushNotifications() {
		print("---- Usuário registrado ----")
	}

	func didFailRegisteringForPushNotifications(error: NGTRequestError) {
		print("---- Erro ao registrar usuário ----")
	}

	func didUnregisterForPushNotifications() {
		print("---- Registro do usuário removido ----")
	}

	func didFailUnregisteringForPushNotifications(error: NGTRequestError) {
		print("---- Erro ao remover registro do usuário ----")
	}

}
```

3. Implemente o protocolo NGTDelegate se quiser receber atualizações sobre registro do usuário.

O protocolo tem 4 métodos:
* func didRegisterForPushNotifications() -> Void
* func didFailRegisteringForPushNotifications() -> NGTRequestError
* func didUnregisterForPushNotifications() -> Void
* func didFailUnregisteringForPushNotifications() -> NGTRequestError

O erro retornado em caso de falha pode ser:
* NGTRequestError.authorizationError - falha na inicialização da sdk ou chave inválida
* NGTRequestError.dataTaskError - falha na requisição
* NGTRequestError.sdkError - erro interno no SDK


##### Funcionalidades

###### Register
Habilita usuário para receber push notifications. Quando achar conveniente (geralmente após o login do usuário), chame o método para registrar o usuário para receber notificações.

Os parâmetros são:
* userKey: identificador do usuário
* apnsToken: token da Apple
* fcmToken: token do Firebase
* completionHandler (opcional): callback


```sh
func login() {
	NGTManager.sharedInstance.registerForPushNotification(
		userKey: <user- key>,
		token: <apns-token>,
		fcmToken: <fcm-token>,
		completionHandler: { [weak self] (error) in
			if error == nil {
				print("---- Usuário registrado ----")
			} else {
				print("---- Erro ao registrar usuário ----")
			}
		})
}
```


###### Unregister

Desabilita o usuário para receber push notifications. Quando achar conveniente (geralmente após o logout do usuário), chame o método para desabilitar o usuário para receber notificações.

Os parâmetros são:
* userKey: identificador do usuário
* apnsToken: token da Apple
* fcmToken: token do Firebase
* completionHandler (opcional): callback

```sh
func logout() {
	NGTManager.sharedInstance.unregisterForPushNotification(
		userKey: <user-key>,
		token: <apns-token>,
		fcmToken: <fcm-token>,
		completionHandler: { [weak self] (error) in
			if error == nil {
				print("---- Registro do usuário removido ----")
			} else {
				print("---- Erro ao remover registro ----")
			}
		})
}
```


###### Callback de recebimentos e interações das notificações.

Notifique o SDK sobre o recebimento da notificação para envio do callback RECEIVED. Você pode fazer isso de duas formas (utilize apenas uma).

1. Para envio do callback de notificação recebida em foreground, adicione a chamada ao NGT no AppDelegate no método:  

```sh 

UNUserNotificationCenterDelegate.userNotificationCenter(_:willPresent: withCompletionHandler:)

func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent 
notification: UNNotification, withCompletionHandler completionHandler: @escaping 
(UNNotificationPresentationOptions) -> Void) {
	let userInfo = notification.request.content.userInfo
	//Notifique ao NGT que uma notificação foi recebida
	//enquanto o app estava aberto
	NGTManager.sharedInstance.didReceiveNotification(userInfo: userInfo)
	completionHandler([.alert, .badge, .sound])
}
```

2. Para envio do callback de notificação recebida em foreground e background (app aberto porém minimizado), você deve criar uma Notification Service Extension e então adicionar essa chamada no método:

```sh
didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void)

override func didReceive(_ request: UNNotificationRequest, withContentHandler
contentHandler: @escaping (UNNotificationContent) -> Void) {
	let userInfo = request.content.userInfo
	//Notifique ao NGT que uma notificação foi recebida
	//enquanto o app estava aberto ou minimizado
	NGTManager.sharedInstance.didReceiveNotification(userInfo: userInfo)
	contentHandler(bestAttemptContent)
}
```

Você também pode notificar o NGT sobre interações do usuário com a push para o envio de callbacks de READ e DISMISS. Faça isso no AppDelegate no método:

```sh
UNUserNotificationCenterDelegate.userNotificationCenter(_:didReceive:withCompletionHandler:).

func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive
response: UNNotificationResponse, withCompletionHandler completionHandler:
@escaping () -> Void) {
	let userInfo = response.notification.request.content.userInfo
	//Notifique ao NGT que o usuário interagiu com a notificação
	//(clicou pra ler ou removeu)
	NGTManager.sharedInstance.handleNotificationResponse(response: response,
	withCompletionHandler: completionHandler)
}
```

O SDK retém callbacks que tenham alguma falha em seu envio ao NGT. Ocorre uma nova tentativa de envio desses callbacks retidos a cada novo callback disparado (no recebimento ou interação com a push).

Caso não queira esperar, você pode verificar se existem pendências e forçar esse envio quando achar conveniente.

Para isso, primeiro chame o método que verifica a existência de callbacks pendentes. E então, o método que força o envio, como no exemplo a seguir:


```sh
func someConvenientMethod() {
	if NGTManager.sharedInstance.hasPendingCallbacks() {
		NGTManager.sharedInstance.forcePendingCallbacks { 
			(error) in
			if error != nil {
				//falha ao enviar
			} else {
				//os eventos pendentes foram enviados
			}
		}
	}
}
```

ATENÇÃO: 
Para permitir que seu app se comunique com uma Extension você precisa de um AppGroup. 
Se ainda não tiver, habilite essa capability tanto para o Main App target como para o target da Notification Extension. Para habilitar, basta clicar em  "target" > "Signing & Capabilities" > "All" > "+ Capability" > "App Groups".

<img src="https://ngt.mobicare.com.br/docs/images/image-appgroup.png" width="400">

Adicione, para cada target (app e extension), um container. Na seção App Groups clique no botão "+" . 

<img src="https://ngt.mobicare.com.br/docs/images/image-containers.png" width="400">

O container deve ser composto da seguinte forma (para ambos):
group.MAIN_APP_BUNDLE_IDENTIFIER.ngtsdk

<img src="https://ngt.mobicare.com.br/docs/images/image-group-container.png" width="400">



## Histórico de lançamentos

* 0.0.1
    * Primeira versão


## Contributing

1. Faça o _fork_ do projeto
2. Crie uma _branch_ para sua modificação
3. Faça o _commit_ 
4. _Push_ 
5. Crie um novo _Pull Request_

[npm-image]: https://img.shields.io/npm/v/datadog-metrics.svg?style=flat-square
[npm-url]: https://npmjs.org/package/datadog-metrics
[npm-downloads]: https://img.shields.io/npm/dm/datadog-metrics.svg?style=flat-square
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
[wiki]: https://github.com/seunome/seuprojeto/wiki
